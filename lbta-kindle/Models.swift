//
//  Models.swift
//  lbta-kindle
//
//  Created by Admin on 5/9/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

import UIKit


class Book:Decodable {
    let title:String
    let author:String
    let pages: [Page]?
   // let image:UIImage?
    let coverImageUrl:String
    
    init(title:String, author:String, pages:Array<Page>,coverImageUrl:String) {
        self.title = title
        self.author = author
        self.pages = pages
        self.coverImageUrl = coverImageUrl
    }
    
}

class Page : Decodable {
    let id:Int
    let text:String
    init(id:Int, text:String) {
        self.id = id
        self.text = text
    }
    
}
