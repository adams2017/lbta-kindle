//
//  BookCell.swift
//  lbta-kindle
//
//  Created by Admin on 5/9/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

import UIKit

class BookCell: UITableViewCell {
    var aBook:Book? {
        didSet {
            guard let aBook = aBook else { return}
            #warning("fix image")
           // self.coverImageView.image = aBook.image
            coverImageView.image = nil 
            getImage(for: aBook.coverImageUrl)
            self.authorLabel.text = aBook.author
            self.titleLabel.text = aBook.title
            print("set auth and title")
            self.backgroundColor = .darkGray
        }
        
    }
    
    func getImage(for url:String ) {
        
        guard let endpoint = URL(string: url) else { return }
        
        

        URLSession.shared.dataTask(with: endpoint , completionHandler: { (data, response, error) in
            if error != nil {
                print("error",error.debugDescription)
            }
            
            let image = UIImage(data: data!)
        
            DispatchQueue.main.async {
                self.coverImageView.image = image 
            }
            
        }).resume()
        
        //return image
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor =  .darkGray
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        [coverImageView,titleLabel,authorLabel].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview($0)
        }
 
        //coverImageView.frame = CGRect(x: 8, y: 8, width: 50, height: 64)
        //titleLabel.frame = CGRect(x: 66, y: 20, width: 200, height: 20)
        NSLayoutConstraint.activate([
            coverImageView.topAnchor.constraint(equalTo: self.topAnchor, constant: 8),
            coverImageView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8),
            coverImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -8),
            coverImageView.widthAnchor.constraint(equalToConstant: 50),
            
            titleLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: -8),
            titleLabel.leftAnchor.constraint(equalTo: coverImageView.rightAnchor, constant: 4),
            titleLabel.heightAnchor.constraint(equalToConstant: 20),
            titleLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -8),
            
            authorLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 4),
            authorLabel.leftAnchor.constraint(equalTo: titleLabel.leftAnchor),
            authorLabel.heightAnchor.constraint(equalToConstant: 20),
            authorLabel.rightAnchor.constraint(equalTo: titleLabel.rightAnchor)
            ])
 
    }
    
    
   private let coverImageView:UIImageView = {
        let ui = UIImageView()
        ui.image = UIImage()
        ui.contentMode = UIView.ContentMode.scaleAspectFill
        ui.clipsToBounds = true
        ui.backgroundColor = .brown
        return ui
    }()
    
   private  let titleLabel:UILabel = {
        let ui = UILabel()
        ui.text = "title lable"
        ui.font = UIFont.systemFont(ofSize: 14)
        ui.textColor = UIColor.white
        ui.backgroundColor = UIColor.darkGray
        ui.numberOfLines = 1
        return ui
    }()
    
    private let authorLabel:UILabel = {
        let ui = UILabel()
        ui.text = String(repeating: "Author Label ", count: 3)
        ui.font = UIFont.systemFont(ofSize: 14)
        ui.textColor = UIColor.white
        ui.backgroundColor = UIColor.darkGray
        ui.numberOfLines = 1
        return ui
    }()



    
}
