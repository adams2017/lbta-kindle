//
//  PageCell.swift
//  lbta-kindle
//
//  Created by Admin on 5/11/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

import UIKit

import UIKit

class PageCell: UICollectionViewCell {
    //Mark: PROPERTIES
    static var reuseIdentifier: String {
        return self.description()
    }
    //MARK: LIFECYCLE
    override init(frame: CGRect) {
        super .init(frame: frame)
        self.backgroundColor = UIColor.darkGray
        setup()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    var aPage:Page? {
        didSet {
            guard let aPage = aPage else {return}
            textLabel.text = aPage.text
        }
        
    }
    func setup() {
        [textLabel ].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview($0)
        }
        
        NSLayoutConstraint.activate([
            textLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 0),
            textLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8),
            textLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0),
            textLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -8)
    
            ])
    }
    //MARK: UI Elements =======================================================
    private let textLabel:UILabel = {
        let ui = UILabel()
        ui.text = "text"
        ui.font = UIFont.systemFont(ofSize: 14)
        ui.textColor = UIColor.black
        ui.backgroundColor = UIColor.lightGray
        ui.numberOfLines = 0
        return ui
    }()

}

