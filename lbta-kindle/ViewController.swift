//
//  ViewController.swift
//  lbta-kindle
//
//  Created by Admin on 5/8/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

import UIKit

// test 
class ViewController: UITableViewController {
    var books:Array<Book>?
    let endpointStr = "https://letsbuildthatapp-videos.s3-us-west-2.amazonaws.com/kindle.json"
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        //setupBooks()
        fetchBooks()
        #warning("new: nav bar styling")
       // let file =  "❌\(#file)".components(separatedBy: "/").last! //print("\n\u{2665} Line \(#line) func \(#function) \(file)\n")
       // print(file)


       // print(" ❌ Error ----> File: \(#file), Function: \(#function), Line: \(#line)")
        navigationController?.navigationBar.barTintColor = UIColor(red: 40/255, green: 40/255, blue: 40/255, alpha: 1)
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.white]
        
    }
    
    func setup() {
     
        let file = "\(#file)".components(separatedBy: "/").last!; print("\n❌ Line \(#line) func \(#function) \(file)\n")
        navigationItem.title = "My Kindle"
        tableView.separatorColor = UIColor(white: 1, alpha: 0.4)
        self.tableView.backgroundColor = UIColor.darkGray
        tableView.register(BookCell.self, forCellReuseIdentifier: "cell")
    }
    
    func fetchBooks() {
        print(self.debugDescription)
        let endpoint = URL(string: endpointStr)!
        
         URLSession.shared.dataTask(with: endpoint, completionHandler: { (data, response, error) in
            
            if error != nil {
                print(error.debugDescription)
                return
            }
            print(response!)
            guard let data = data else { return }

            let dataString = String(bytes: data, encoding: .utf8)
            print(dataString ?? "")
            self.books = []
            do {
               self.books = try JSONDecoder().decode(Array<Book>.self, from: data)
            } catch let err {
                print("Error: \(err)")
                fatalError("JSON Decoder failed")
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }).resume()
        
    }
    
    /*
    func setupBooks() {
        books = []
        let pages1 = [ Page(number: 1, text: "This is the first page of SJ book"),
                    Page(number: 2, text: "This is second page of SJ book")]
        let pages2 = [ Page(number: 1, text: "Page 1 of BG book"),
                    Page(number: 2, text: "Page 2 of BG book")]
        let pages3 = [ Page(number: 1, text: "Page 1 of EM book"), Page(number: 2, text: "Page 2 of EM book")]
        
        
        let book1 = Book(title: "Steve Jobs Bio", author: "Walter Isacson", pages: pages1,
                         image: UIImage(named: "steve_jobs")!)
        let book2 = Book(title: "Bill Gates", author: "A Author", pages: pages2,
                         image: UIImage(named: "bill_gates")!)
        let book3 = Book(title: "Elon Musk Biography", author: "E Author", pages: pages3,
                         image: UIImage(named: "elon_musk2")!)
        books?.append(book1)
        books?.append(book2)
        books?.append(book3)
    }
    */
    
    override func  tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
        
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return books?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 60
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! BookCell
        let row = indexPath.row
 
        guard let books = books else {
            fatalError("books is nil")
        }
        
        cell.aBook = books[row]
        
        /*
        cell.textLabel?.text = books?[row].title
        cell.imageView?.image = books?[row].image
        cell.imageView?.clipsToBounds = true
 */
        
        return cell
    }
 
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let v = UIView()
        v.backgroundColor = UIColor(red: 40/255, green: 40/255, blue: 40/255, alpha: 1)
        let sc = UISegmentedControl(items: ["Cloud","Device"])
        sc.tintColor = .white
        sc.selectedSegmentIndex = 0
        sc.translatesAutoresizingMaskIntoConstraints = false
        v.addSubview(sc)
        NSLayoutConstraint.activate([
            //sc.topAnchor.constraint(equalTo: v.topAnchor),
            sc.widthAnchor.constraint(equalToConstant: 200),
            sc.centerXAnchor.constraint(equalTo: v.centerXAnchor),
            sc.centerYAnchor.constraint(equalTo: v.centerYAnchor),
            sc.heightAnchor.constraint(equalToConstant: 20)])
        return v
    }
    #warning("new: table view row selected")
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let book = books?[indexPath.row]
        print("123 ", indexPath.row)
        let flow = UICollectionViewFlowLayout()
        flow.scrollDirection = .horizontal
        flow.minimumLineSpacing = 0
        
        let bookPagerVC = BookPagerVC(collectionViewLayout: flow)
        bookPagerVC.book = book
    
        bookPagerVC.collectionView.isPagingEnabled = true 
        let nav = UINavigationController(rootViewController: bookPagerVC)
        present(nav, animated: true, completion: nil)
    }
    
}

/*
 //in setup
 tableView.separatorColor = UIColor(white: 1, alpha: 0.4)
 self.tableView.backgroundColor = UIColor.darkGray
 tableView.register(BookCell.self, forCellReuseIdentifier: "cell")
 //functions
 override func  tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
 return 80
 }
 override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
 return books?.count ?? 0
 }
 override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
 return 60
 }
 override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
 let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! BookCell
 let row = indexPath.row
 cell.textLabel?.text = books?[row].title
 cell.imageView?.image = books?[row].image
 cell.imageView?.clipsToBounds = true
 return cell
 }
 override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
 let v = UIView()
 return v
 }
 override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
return
 }
 
 
 
 */

func printLog(_ message: String, file: String = #file, function: String = #function, line: Int = #line) {
    #if DEVELOPMENT
    let className = file.components(separatedBy: "/").last
    print(" ❌ Error ----> File: \(className ?? ""), Function: \(function), Line: \(line), Message: \(message)")
    #endif
}
