//
//  BookPagerVC.swift
//  lbta-kindle
//
//  Created by Admin on 5/11/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

import UIKit

// R
// N
// S
// C
// D
// D

class BookPagerVC : UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var book:Book?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.backgroundColor = .blue
        navigationItem.title = self.book?.title
        #warning("new: adding left nav bar button text only")
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Close", style: .plain, target: self, action: #selector(closeTouchSelector))
        collectionView.register(PageCell.self, forCellWithReuseIdentifier: PageCell.reuseIdentifier)
    }   
    @objc func closeTouchSelector() {
        print("123")
        dismiss(animated: true, completion: nil)
    }
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return book?.pages?.count ?? 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PageCell.reuseIdentifier, for: indexPath) as! PageCell
        cell.backgroundColor = .gray
        cell.aPage = book?.pages?[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height)
    }
}
